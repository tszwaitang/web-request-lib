package com.baselib.webrequestlibdemo

import com.baselib.webrequest.SingleSourceRetrofitManager
import com.baselib.webrequestlibdemo.webrequest.GetDataService


class ApiHelper :SingleSourceRetrofitManager() {
    private lateinit var getDataService: GetDataService;
    init {
        DEBUG = true
    }
    companion object {
        @Volatile
        private var instance: ApiHelper? = null
        fun getInstance() =
            instance ?: synchronized(this) {
                instance ?: ApiHelper().also { instance = it }.also {
                    it.getRetrofit(BuildConfig.SERVER_URL)
                    it.createApiService()
                }
            }
    }

    /*
    for creating all api service
     */
    private fun createApiService() {
        getDataService = retrofit!!.create(GetDataService::class.java)
    }
}
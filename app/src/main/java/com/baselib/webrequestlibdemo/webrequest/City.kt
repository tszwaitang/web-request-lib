package com.baselib.webrequestlibdemo.webrequest

import com.google.gson.annotations.SerializedName

class City {
    /**
     * id : 1
     * cityCode : 1819729
     * name : hong kong
     */
    @SerializedName("id")
    private var id = 0

    @SerializedName("cityCode")
    private var cityCode = 0

    @SerializedName("name")
    private var name: String? = null

    fun getId(): Int {
        return id
    }

    fun setId(id: Int) {
        this.id = id
    }

    fun getCityCode(): Int {
        return cityCode
    }

    fun setCityCode(cityCode: Int) {
        this.cityCode = cityCode
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String?) {
        this.name = name
    }
}
package com.baselib.webrequestlibdemo.webrequest

import com.baselib.webrequest.BaseResponse
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver
import retrofit2.http.GET
import retrofit2.http.POST


interface GetDataService {
    @GET("weather/city")
    fun getCityList(): Single<BaseResponse<List<City>>>
}
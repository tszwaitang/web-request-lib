package com.baselib.webrequestlibdemo

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.baselib.webrequest.BaseResponse
import com.baselib.webrequest.CusSO
import com.baselib.webrequest.RetryWithDelay
import com.baselib.webrequestlibdemo.webrequest.City
import com.baselib.webrequestlibdemo.webrequest.GetDataService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import com.baselib.webrequest.call

class DataRepository(var apiHelper: ApiHelper) {
    companion object{
        const val TAG = "DataRepository"
    }
    private var getDataService =
        apiHelper?.getRetrofit(BuildConfig.SERVER_URL)?.create(GetDataService::class.java)


    fun getData(disposables: CompositeDisposable?): MutableLiveData<List<City>> {
        var data = MutableLiveData<List<City>>();
        val request = getDataService
            ?.getCityList()
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : CusSO<BaseResponse<List<City>>>() {
                override fun successHandler(t: BaseResponse<List<City>>) {
                    Log.d(TAG, "onSuccess: ${t.toString()}")
                    data.value = t.data
                }

                override fun errorHandler(e: Throwable) {
                    Log.e(TAG, "onError:", e)
                }

            })
        request?.let { disposables?.add(it) }
        return data
    }

    fun getDataWithExtension(disposables: CompositeDisposable?): MutableLiveData<List<City>> {
        var data = MutableLiveData<List<City>>();
        val request = getDataService.getCityList()
            ?.call(object : CusSO<BaseResponse<List<City>>>() {
                override fun successHandler(t: BaseResponse<List<City>>) {
                    Log.d(TAG, "onSuccess: ${t.toString()}")
                    data.value = t.data
                }

                override fun errorHandler(e: Throwable) {
                    Log.e(TAG, "onError:", e)
                }

            })
        request?.let { disposables?.add(it) }
        return data
    }

    fun getDataWithRetry(disposables: CompositeDisposable?): MutableLiveData<List<City>> {
        var data = MutableLiveData<List<City>>();
        val request = getDataService
            ?.getCityList()
            ?.call(object: RetryWithDelay(){}, object : CusSO<BaseResponse<List<City>>>() {
                override fun successHandler(t: BaseResponse<List<City>>) {
                    Log.d(TAG, "onSuccess: ${t.toString()}")
                    data.value = t.data
                }

                override fun errorHandler(e: Throwable) {
                    Log.e(TAG, "onError:", e)
                }
            })
        request?.let { disposables?.add(it) }
        return data
    }
}

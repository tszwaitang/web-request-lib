package com.baselib.webrequestlibdemo

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import io.reactivex.disposables.CompositeDisposable


class MainActivity : AppCompatActivity() {
    companion object {
        const val TAG = "MainActivity"
    }

    var apiHelper: ApiHelper = ApiHelper.getInstance()

    var disposable: CompositeDisposable? = CompositeDisposable()

    var dataRepository: DataRepository = DataRepository(apiHelper)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        dataRepository.getDataWithRetry(disposable).observe(this, Observer {
            Log.d(TAG, "observe: ${it.toString()}")
        })


    }
}


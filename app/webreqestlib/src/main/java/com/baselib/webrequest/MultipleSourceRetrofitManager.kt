//package com.baselib.webrequest
//
//import okhttp3.OkHttpClient
//import retrofit2.Retrofit
//
//open class MultipleSourceRetrofitManager {
//    private var retrofitMap: MutableMap<String, SingleSourceRetrofitManager> = mutableMapOf()
//
//    companion object {
//        var DEBUG = false
//    }
//
//    fun <T : Any> initRetrofits(urlList: List<String>, apiList: List<T>) {
//        urlList.forEach {
//            retrofitMap[it] = SingleSourceRetrofitManager()
//            retrofitMap[it]?.getRetrofit(it)
//            apiList.forEach {
//                retrofitMap[it]
//            }
//        }
//    }
//
//    private fun checkRetrofit() {
//        if (retrofit != null) {
//            throw Exception("retrofit not init")
//        }
//    }
//
//    open fun <S> createApi(serviceClass: Class<S>, client: OkHttpClient?): S {
//        var baseURL = ""
//        try {
//            val field1: Field = serviceClass.getField("baseURL")
//            baseURL = field1.get(serviceClass) as String
//        } catch (e: NoSuchFieldException) {
//            e.printStackTrace()
//        } catch (e: IllegalAccessException) {
//            e.message
//            e.printStackTrace()
//        }
//        return if (retrofit != null && retrofit!!.baseUrl().host === baseURL) {
//            retrofit!!.create(serviceClass)
//        } else {
//            getRetrofit(baseURL).create(serviceClass)
//        }
//    }
//}
